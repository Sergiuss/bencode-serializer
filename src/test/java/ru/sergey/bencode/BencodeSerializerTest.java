package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.Mockito;

public class BencodeSerializerTest {
    @Test
    public void complexTypeTest() {
        final List<Object> objects = new ArrayList<>();
        final Map<String, Object> dict = new HashMap<>();
        final List<Object> list = new ArrayList<>();

        objects.add(1);
        objects.add("test_string");
        objects.add(dict);
        objects.add(new byte[] { 'i', '5', 'e', ':' });
        objects.add(list);
        objects.add("end");
        objects.add(new Object[] { "string", 890L });
        objects.add(new Object[] { "sss", "456" });

        dict.put("list", list);
        dict.put("key", "value");
        dict.put("int", 123);

        list.add("str");
        list.add(999);
        list.add(new Short((short) 5));
        list.add(new Byte((byte) 8));

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(objects, os);

        final String bencodedString = os.toString();

        final Object obj = BencodeSerializer
                .deserialize(new ByteArrayInputStream(os.toByteArray()));

        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(obj, os);

        assertEquals(bencodedString, os.toString());
    }

    @Test
    public void decodeTest() {
        final String bencodedString = "d9:publisher3:bob17:publisher-webpage15:www.example.com18:publisher.location4:homee";

        final Object obj = BencodeSerializer
                .deserialize(new ByteArrayInputStream(bencodedString.getBytes()));

        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(obj, os);

        assertEquals(bencodedString, os.toString());
    }

    @Test(expected = SerializationException.class)
    public void serializeNullTest() {
        BencodeSerializer.serialize((Object) null, new ByteArrayOutputStream());
    }

    @Test(expected = SerializationException.class)
    public void serializeUnsupportedType() {
        BencodeSerializer.serialize(new Date(), new ByteArrayOutputStream());
    }

    @Test
    public void deserializeEmptyStream() {
        final Object value = BencodeSerializer.deserialize(new ByteArrayInputStream(new byte[0]));
        assertNull(value);
    }

    @Test(expected = SerializationException.class)
    public void deserializeFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.doThrow(IOException.class).when(is).read();

        BencodeSerializer.deserialize(is);
    }

    @Test
    public void serializerFacadeTest() {
        // byte[]
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(new byte[] { 5, 8, 9 }, os);

        checkResults(os.toString());

        // map
        final Map<String, Object> dict = new HashMap<>();
        dict.put("key", "value");
        dict.put("int", 123);

        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(dict, os);

        checkResults(os.toString());

        // int
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(123, os);

        checkResults(os.toString());

        // long
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(123L, os);

        checkResults(os.toString());

        // short
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize((short)123, os);

        checkResults(os.toString());

        // byte
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize((byte)8, os);

        checkResults(os.toString());

        // Integer
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(new Integer(1000), os);

        checkResults(os.toString());

        // Long
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(new Long(80000), os);

        checkResults(os.toString());

        // Short
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(new Short((short)5), os);

        checkResults(os.toString());

        // Byte
        os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(new Byte((byte)8), os);

        checkResults(os.toString());
    }

    private void checkResults(final String bencodedString) {
        final Object deserializedObj = BencodeSerializer.deserialize(new ByteArrayInputStream(bencodedString.getBytes()));

        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        BencodeSerializer.serialize(deserializedObj, os);

        assertEquals(bencodedString, os.toString());
    }
}
