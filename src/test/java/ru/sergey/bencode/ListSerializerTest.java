package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static ru.sergey.bencode.EscapeSymbols.LIST;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.Mockito;

public class ListSerializerTest {
    private static final String BENCODED_STR = "l6:Stringi123ed3:key5:valueel3:newee";

    @Test
    public void serializeListTest() throws IOException {
        final List<Object> list = new ArrayList<>();

        final Map<String, Object> map = new HashMap<>();
        map.put("key", "value");

        final List<Object> l = new ArrayList<>();
        l.add("new");

        list.add("String");
        list.add(123);
        list.add(map);
        list.add(l);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        ListSerializer.serialize(list, os);

        assertEquals(BENCODED_STR, os.toString());

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        // clean list marker
        is.read();

        final Collection<Object> obj = ListSerializer.deserialize(is);

        os = new ByteArrayOutputStream();

        ListSerializer.serialize(obj, os);

        assertEquals(BENCODED_STR, os.toString());
    }

    @Test(expected = SerializationException.class)
    public void writeToWrongStream() throws IOException {
        final List<Object> list = new ArrayList<>();
        final OutputStream os = Mockito.mock(OutputStream.class);

        Mockito.doThrow(IOException.class).when(os).write(LIST);

        ListSerializer.serialize(list, os);
    }
}
