package ru.sergey.bencode;

import static org.junit.Assert.assertEquals;
import static ru.sergey.bencode.EscapeSymbols.INTEGER;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Test;
import org.mockito.Mockito;

public class IntegerSerializerTest {
    @Test
    public void integerSerializerTest() throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();

        IntegerSerializer.serialize(Long.MIN_VALUE, os);
        IntegerSerializer.serialize(Long.MAX_VALUE, os);
        IntegerSerializer.serialize(0L, os);

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        // skip type
        is.read();
        assertEquals(Long.MIN_VALUE, IntegerSerializer.deserialize(is));
        is.read();
        assertEquals(Long.MAX_VALUE, IntegerSerializer.deserialize(is));
        is.read();
        assertEquals(0L, IntegerSerializer.deserialize(is));
    }

    @Test(expected = SerializationException.class)
    public void emptyInputStreamDeserialize() {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        os.write(INTEGER);

        final InputStream is = new ByteArrayInputStream(os.toByteArray());

        IntegerSerializer.deserialize(is);
    }

    @Test(expected = SerializationException.class)
    public void serializeToClosedStream() throws IOException {
        final OutputStream os = Mockito.mock(OutputStream.class);
        Mockito.doThrow(IOException.class).when(os).write(INTEGER);

        IntegerSerializer.serialize(Long.MIN_VALUE, os);
    }

    @Test(expected = SerializationException.class)
    public void deserializeFromClosedStream() throws IOException {
        final InputStream is = Mockito.mock(InputStream.class);
        Mockito.doThrow(IOException.class).when(is).read();

        IntegerSerializer.deserialize(is);
    }
}
