package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.EOF;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MAX;
import static ru.sergey.bencode.EscapeSymbols.LENGTH_MIN;
import static ru.sergey.bencode.EscapeSymbols.SEPARATOR;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * Byte strings.
 *
 * A byte string (a sequence of bytes, not necessarily characters) is encoded as
 * <length>:<contents>. The length is encoded in base 10, like integers, but
 * must be non-negative (zero is allowed); the contents are just the bytes that
 * make up the string.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Bencode">Bencode types: Byte
 *      strings</a>
 */
final class ByteStringSerializer {
    private ByteStringSerializer() {}

    /**
     * @param firstDigit
     *            first digit of the number (read externally for type detection)
     * @param is
     *            any input stream
     * @return length of the next byte string
     */
    static int getLength(final char firstDigit, final InputStream is) {
        // empty string, len == 0
        if (firstDigit == LENGTH_MIN) {
            return 0;
        }

        try {
            final StringBuilder sb = new StringBuilder();
            int c = firstDigit;

            do {
                if (c == EOF) {
                    throw new SerializationException("Unexpected end of file");
                }

                final char digit = (char)c;

                if (digit < LENGTH_MIN || digit > LENGTH_MAX) {
                    throw new SerializationException("Incorrect digit");
                }

                sb.append(digit);
            } while ((c = is.read()) != SEPARATOR);

            return Integer.parseInt(sb.toString());
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    static void serialize(final String value, final OutputStream os) {
        serialize(value.getBytes(StandardCharsets.UTF_8), os);
    }

    static void serialize(final byte[] value, final OutputStream os) {
        try {
            os.write(Integer.toString(value.length).getBytes());
            os.write(SEPARATOR);
            os.write(value);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    static String deserialize(final int len, final InputStream is) {
        return new String(deserialize(is, len), StandardCharsets.UTF_8);
    }

    static byte[] deserialize(final InputStream is, final int len) {
        try {
            if (is.available() < len) {
                throw new SerializationException(
                        "InputStream is smaller than read size");
            }

            final byte[] buffer = new byte[len];

            is.read(buffer, 0, len);

            return buffer;
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }
}
