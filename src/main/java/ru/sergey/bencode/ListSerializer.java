package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.END;
import static ru.sergey.bencode.EscapeSymbols.LIST;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Lists.
 *
 * Lists are encoded as follows: l<bencoded values>e The initial l and trailing
 * e are beginning and ending delimiters. Lists may contain any bencoded type,
 * including integers, strings, dictionaries, and even lists within other lists.
 *
 * @see <a href="https://wiki.theory.org/BitTorrentSpecification#Lists">Bencode types: Lists</a>
 */
final class ListSerializer {
    private ListSerializer() {}

    static void serialize(final Iterable<Object> value, final OutputStream os) {
        try {
            os.write(LIST);

            for (final Object listValue : value) {
                BencodeSerializer.serialize(listValue, os);
            }

            os.write(END);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    static Collection<Object> deserialize(final InputStream is) {
        final Collection<Object> list = new ArrayList<>();

        Object value;

        while ((value = BencodeSerializer.deserialize(is)) != null) {
            list.add(value);
        }

        return list;
    }
}
