package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.DICTIONARY;
import static ru.sergey.bencode.EscapeSymbols.END;
import static ru.sergey.bencode.EscapeSymbols.EOF_TYPE;
import static ru.sergey.bencode.EscapeSymbols.INTEGER;
import static ru.sergey.bencode.EscapeSymbols.LIST;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Map;

public final class BencodeSerializer {
    @SuppressWarnings("unchecked")
    public static void serialize(final Object value, final OutputStream os) {
        if (value == null) {
            throw new SerializationException("Null objects is not supported");
        }

        if (value instanceof String) {
            ByteStringSerializer.serialize((String) value, os);
        } else if (value instanceof byte[]) {
            ByteStringSerializer.serialize((byte[]) value, os);
        } else if (value instanceof Map) {
            DictionarySerializer.serialize((Map<String, Object>) value, os);
        } else if (value instanceof Iterable) {
            ListSerializer.serialize((Iterable<Object>) value, os);
        } else if (value.getClass().isArray()) {
            ListSerializer.serialize(Arrays.asList((Object[])value), os);
        } else if (value instanceof Integer
                || value instanceof Long
                || value instanceof Short
                || value instanceof Byte) {
            IntegerSerializer.serialize(((Number) value).longValue(), os);
        } else {
            throw new SerializationException("Unsupported class type");
        }
    }

    public static void serialize(final String value, final OutputStream os) {
        ByteStringSerializer.serialize(value, os);
    }

    public static void serialize(final byte[] value, final OutputStream os) {
        ByteStringSerializer.serialize(value, os);
    }

    public static void serialize(final Long value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final Integer value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final Short value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final Byte value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final long value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final int value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final short value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final byte value, final OutputStream os) {
        IntegerSerializer.serialize(value, os);
    }

    public static void serialize(final Iterable<Object> value, final OutputStream os) {
        ListSerializer.serialize(value, os);
    }

    public static void serialize(final Map<String, Object> value, final OutputStream os) {
        DictionarySerializer.serialize(value, os);
    }

    public static Object deserialize(final InputStream is) {
        final char type = getType(is);

        switch (type) {
        case DICTIONARY:
            return DictionarySerializer.deserialize(is);

        case END:
            return null;

        case INTEGER:
            return IntegerSerializer.deserialize(is);

        case LIST:
            return ListSerializer.deserialize(is);

        case EOF_TYPE:
            return null;

        default:
            final int len = ByteStringSerializer.getLength(type, is);

            return ByteStringSerializer.deserialize(len, is);
        }
    }

    private static char getType(final InputStream is) {
        try {
            return (char) is.read();
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }
}
