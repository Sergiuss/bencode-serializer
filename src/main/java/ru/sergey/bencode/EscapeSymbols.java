package ru.sergey.bencode;

public final class EscapeSymbols {
    private EscapeSymbols() {}

    public static final int EOF = -1;

    public static final char EOF_TYPE = (char)EOF;

    public static final char INTEGER = 'i';

    public static final char LIST = 'l';

    public static final char DICTIONARY = 'd';

    public static final char END = 'e';

    public static final char LENGTH_MIN = '0';

    public static final char LENGTH_MAX = '9';

    public static final char SEPARATOR = ':';
}
