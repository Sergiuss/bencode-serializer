package ru.sergey.bencode;

/**
 * Indicates an error during serialization.
 */
public class SerializationException extends RuntimeException {
    private static final long serialVersionUID = 2816154105883147481L;

    public SerializationException() {
        super();
    }

    public SerializationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SerializationException(final String message) {
        super(message);
    }

    public SerializationException(final Throwable cause) {
        super("", cause);
    }
}
