package ru.sergey.bencode;

import static ru.sergey.bencode.EscapeSymbols.DICTIONARY;
import static ru.sergey.bencode.EscapeSymbols.END;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dictionaries
 *
 * Dictionaries are encoded as follows: d<bencoded string><bencoded element>e
 * The initial d and trailing e are the beginning and ending delimiters. Note
 * that the keys must be bencoded strings. The values may be any bencoded type,
 * including integers, strings, lists, and other dictionaries. Keys must be
 * strings and appear in sorted order (sorted as raw strings, not
 * alphanumerics). The strings should be compared using a binary comparison, not
 * a culture-specific "natural" comparison.
 *
 * @see <a href="https://wiki.theory.org/BitTorrentSpecification#Dictionaries" />Bencode types: Dictionaries</a>
 */
final class DictionarySerializer {
    private DictionarySerializer() {}

    static void serialize(final Map<String, Object> value, final OutputStream os) {
        try {
            os.write(DICTIONARY);

            // sort keys
            final List<String> keys = new ArrayList<>(value.keySet());
            Collections.sort(keys);

            for (int i = 0, n = keys.size(); i < n; i++) {
                final String key = keys.get(i);
                final Object dictValue = value.get(key);

                // serialize key
                BencodeSerializer.serialize(key, os);
                // serialize value
                BencodeSerializer.serialize(dictValue, os);
            }

            os.write(END);
        } catch (final IOException e) {
            throw new SerializationException(e);
        }
    }

    static Map<String, Object> deserialize(final InputStream is) {
        final Map<String, Object> dictionary = new HashMap<>();

        String key;
        Object value;

        while ((key = (String)BencodeSerializer.deserialize(is)) != null) {
            value = BencodeSerializer.deserialize(is);

            dictionary.put(key, value);
        }

        return dictionary;
    }
}
